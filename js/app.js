function generatePassword(passwordLength) {

  var charList = ["a","b","c","d","e","f","g","h","j","k","m","n","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","J","K","M","N","P","Q","R","S","T","U","V","W","X","Y","Z","2","3","4","5","6","7","8","9","!","@","#","$","%","&","*","+","=","-","_","~","/","\\","?"];

  var password = "";
  
  var target = document.getElementById("password");

  for (i = 0; i < passwordLength; i++) {
    var randomCharacter = charList[Math.floor(Math.random() * charList.length)];
    password = password + randomCharacter;
  }

  //console.log(password);
  target.innerHTML = password;
}

var toggleElement = document.getElementsByClassName("center");
var clipboard = new Clipboard('.copy');

function toggleClass(cl) {
  document.getElementById("password-container").classList.toggle(cl);
  document.getElementById("alert").classList.toggle(cl);
}

clipboard.on('success', function(e) {
  toggleClass("hidden");
  setTimeout(toggleClass, 500, "hidden");
  e.clearSelection();
});

function getQueryValue(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){
      return decodeURIComponent(pair[1]);
    }
  }
  return(false);
}

if (getQueryValue("length") == false) {
  generatePassword(12);
} else {
  generatePassword(getQueryValue("length"));
}



document.getElementById("generate").addEventListener("click", function(){generatePassword(12);});
